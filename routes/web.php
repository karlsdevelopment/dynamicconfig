<?php

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

Route::get('/clear-cache', function () {
    return Cache::store('array')->tags(Config::get('cache_tag'))->flush() ? 'Alles schick!' : 'Nicht mit mir!!!';
});