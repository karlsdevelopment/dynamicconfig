<?php

namespace Karls\DynamicConfig\Tests;

use Karls\DynamicConfig\DynamicConfigServiceProvider;

class TestCase extends \Tests\TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    protected function getPackageProviders($app): array
    {
        return [
            DynamicConfigServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        // perform environment setup
    }
}