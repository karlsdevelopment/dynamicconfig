<?php
declare (strict_types=1);

namespace Karls\DynamicConfig\Tests\Unit;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Karls\DynamicConfig\Models\DynamicConfig;
use Karls\DynamicConfig\Tests\TestCase;

class DynamicConfigTest extends TestCase
{
    public function setup(): void
    {
        parent::setUp();
        Schema::create('dynamic_configs', function (Blueprint $t) {
            $t->temporary();
            $t->uuid('key')->primary();
            $t->string('type');
            $t->string('value')->nullable();
        });
    }
    /**
     * @dataProvider check_data
     */
    public function test_dynamicConfig_check_true($key, $value, $type)
    {
        DynamicConfig::create([
            'key' => $key,
            'value' => $value,
            'type' => $type
        ]);

        self::assertTrue(DynamicConfig::check($key, $value));

        DynamicConfig::where('key', $key)->delete();
    }

    /**
     * @dataProvider check_incorrect_data
     */
    public function test_dynamicConfig_check_false($key, $value, $expected, $type)
    {
        DynamicConfig::create([
            'key' => $key,
            'value' => $value,
            'type' => $type
        ]);

        self::assertFalse(DynamicConfig::check($key, $expected));

        DynamicConfig::where('key', $key)->delete();
    }

    public function test_dynamicConfig_key_does_not_exist()
    {
        self::assertFalse(DynamicConfig::check('test', null));
    }

    public function check_data(): array
    {
        return [
            ['test1', 'aaa', 'string'],
            ['test2', true, 'bool'],
            ['test3', 23, 'int'],
            ['test4', 2.4, 'float']
        ];
    }

    public function check_incorrect_data(): array
    {
        return [
            ['test1', 32, 33, 'int'],
            ['test2', 'test', 33, 'string'],
            ['test2', 33, 33, 'string']
        ];
    }
}
