<?php

return [
    'dynamic_config_cache_timeout' => env('DYNAMIC_CONFIG_CACHE_TIMEOUT', 3600),
    'cache_tag' => env('DYNAMIC_CONFIG_CACHE_TAG', 'dynamicconfig')
];