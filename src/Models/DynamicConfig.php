<?php

namespace Karls\DynamicConfig\Models;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

/**
 * Class DynamicConfig
 * @package Karls\DynamicConfig\Models
 *
 * @property string type
 * @property mixed value
 * @property Carbon updatedAt
 *
 * @method static where(string $string, $value)
 */
class DynamicConfig extends Model
{
    public $timestamps = false;
    public $keyType = 'string';
    public $incrementing = false;
    protected $primaryKey = 'key';

    protected $fillable = [
        'key',
        'value',
        'type'
    ];

    protected $dates = ['updatedAt'];

    public static function get(string $key): mixed
    {
        try {
            return self::getValue($key);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return null;
        }
    }

    /**
     * @throws Exception
     */
    private static function getValue(string $key): mixed
    {
        return Cache::store('array')->tags([Config::get('dynamicconfig.cache_tag')])->remember($key, now()->addSeconds(Config::get('dynamicconfig.dynamic_config_cache_timeout')), function () use ($key) {
            $config = self::where('key', $key)->first()
                ?? throw new Exception('Key not found.');
            return self::convertValue($config->value, $config->type);
        });
    }

    public static function convertValue($value, $type): mixed
    {
        switch ($type) {
            case 'string':
                $value = '' . $value;
                break;
            case 'int':
                $value = (int)$value;
                break;
            case 'bool':
                $value = (bool)$value;
                break;
            case 'float':
                $value = (float)$value;
                break;
            default:
                break;
        }

        return $value;
    }

    public static function check(string $key, mixed $value): bool
    {
        try {
            return self::getValue($key) === $value;
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }
}
