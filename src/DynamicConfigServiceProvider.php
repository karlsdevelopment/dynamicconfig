<?php
declare (strict_types=1);

namespace Karls\DynamicConfig;

use Illuminate\Support\ServiceProvider;

class DynamicConfigServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->publishes([
            __DIR__.'/../config/dynamicconfig.php' => config_path('dynamicconfig.php'),
        ]);
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/dynamicconfig.php', 'dynamicconfig'
        );
    }
}